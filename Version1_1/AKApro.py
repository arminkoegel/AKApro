# -*- coding: utf-8 -*-

__author__ 	= u'Armin Kögel'
__contact__	= u'arminkoegel@hotmail.de'
__version__ = '1.1'

"""
Getestet unter Windows 7 und 8 mit
Python 2.7.9
OpenCV 2.4.11
Numpy  1.9.2
"""

print 'lade...'

import os
import sys
import copy
import cv2
import numpy as np
from Tkinter import *
from tkMessageBox import *

#global variables
corner1 = None
corner2 = None
corner3 = None
corner4 = None

scaling_matrix = None
container_width = None
container_height = None

foler = None
infile = None
outfile = None


# scale_container_rectangular ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def scale_container_rectangular(img):
	global scaling_matrix
	set_scaling_parameters(img)
	
	return cv2.warpPerspective(img, scaling_matrix, (container_width, container_height)) # here the transformation is done
# scale_container_rectangular --------------------------------------------------------

# set_corner ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def set_corner1(event, x, y, flags, params):
	global corner1
	if event == cv2.EVENT_LBUTTONDOWN:
		corner1 = [x,y]
		cv2.destroyAllWindows()

def set_corner2(event, x, y, flags, params):
	global corner2
	if event == cv2.EVENT_LBUTTONDOWN:
		corner2 = [x,y]
		cv2.destroyAllWindows()

def set_corner3(event, x, y, flags, params):
	global corner3
	if event == cv2.EVENT_LBUTTONDOWN:
		corner3 = [x,y]
		cv2.destroyAllWindows()	
		
def set_corner4(event, x, y, flags, params):
	global corner4
	if event == cv2.EVENT_LBUTTONDOWN:
		corner4 = [x,y]
		cv2.destroyAllWindows()
# set_corner --------------------------------------------------------
		
# set_scaling_parameters ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def set_scaling_parameters(img):
	global corner1, corner2, corner3, corner4
	global scaling_matrix, container_width, container_height

	print u"\nDer gewünschte Ausschnitt wird in ein Rechteck transformiert."

	img_temp = copy.copy(img)

	while True:

		print u"\nAuf die Ecken des gewünschten Ausschnitt klicken."
		print u"Beginne links oben, weiter im Uhrzeigersinn!"

		cv2.namedWindow('Transformieren', cv2.WINDOW_NORMAL)					# show the whole image
		cv2.imshow('Transformieren', img_temp)
		
		cv2.setMouseCallback('Transformieren',set_corner1)		# if there is a mouse callback, call set_corner
		cv2.waitKey(0)
		cv2.circle(img_temp,(corner1[0],corner1[1]), 20, 255, -1)
		cv2.namedWindow('Transformieren', cv2.WINDOW_NORMAL)					# show the whole image
		cv2.imshow('Transformieren', img_temp)

		cv2.setMouseCallback('Transformieren',set_corner2)		# if there is a mouse callback, call set_corner
		cv2.waitKey(0)
		cv2.circle(img_temp,(corner2[0],corner2[1]), 20, 255, -1)
		cv2.namedWindow('Transformieren', cv2.WINDOW_NORMAL)					# show the whole image
		cv2.imshow('Transformieren', img_temp)
		
		cv2.setMouseCallback('Transformieren',set_corner3)		# if there is a mouse callback, call set_corner
		cv2.waitKey(0)
		cv2.circle(img_temp,(corner3[0],corner3[1]), 20, 255, -1)
		cv2.namedWindow('Transformieren', cv2.WINDOW_NORMAL)					# show the whole image
		cv2.imshow('Transformieren', img_temp)
		
		cv2.setMouseCallback('Transformieren',set_corner4)		# if there is a mouse callback, call set_corner
		cv2.waitKey(0)
		cv2.circle(img_temp,(corner4[0],corner4[1]), 20, 255, -1)
		cv2.namedWindow('Transformieren', cv2.WINDOW_NORMAL)					# show the whole image
		cv2.imshow('Transformieren', img_temp)
	
		cv2.destroyAllWindows()

		pts1 = np.float32([corner1, corner2, corner4, corner3])						#corner4 comes before corner3 for clockwise sequence
		container_width = (corner2[0]+corner3[0])/2 - (corner1[0]+corner4[0])/2		#get the mean width and height of the container
		container_height = (corner4[1]+corner3[1])/2 - (corner1[1]+corner2[1])/2
		pts2 = np.float32([[0,0],[container_width,0],[0,container_height],[container_width,container_height]])
		scaling_matrix = cv2.getPerspectiveTransform(pts1,pts2)									# pts1 is the original scaling, pts2 is the scaling you want to have
		dst = cv2.warpPerspective(img,scaling_matrix,(container_width, container_height))

		print u"\nAusschnittbreite (px): ", container_width, " , Ausschnitthoehe (px): ", container_height

		print u"\n\nPasst das Ergebnis?"
		print u"Bestätigen: Leertaste \nWiederholung: Beliebige Taste\n"

		cv2.namedWindow('Transformieren', cv2.WINDOW_NORMAL)
		cv2.imshow('Transformieren', dst)
		ans = cv2.waitKey(0)							# wait for pressing space. If anything else is pressed, stay in while-loop
		cv2.destroyAllWindows()

		if ans == 32:			# 121 is the code for space. The while-loop stops when space is pressed
			scaling_matrix = scaling_matrix			# set the scaling parameters in global_parameters
			container_height = container_height
			container_width = container_width
			print u"Bildausschnitt transformiert!\n\n"
			break

		img_temp = copy.copy(img)
		cv2.circle(img_temp,(corner1[0],corner1[1]), 20, 255, 3)
		cv2.circle(img_temp,(corner2[0],corner2[1]), 20, 255, 3)
		cv2.circle(img_temp,(corner3[0],corner3[1]), 20, 255, 3)
		cv2.circle(img_temp,(corner4[0],corner4[1]), 20, 255, 3)
		
# set_scaling_parameters ----------------------------------------------------------

def nothing(input):
	pass
	
# go_on +++++++++++++++++++++++++++++++++++++++++	
def go_on():
	global infile
	global outfile
	global e1
	global e2
	global master
	print(u"\nEingangsdatei: %s\nAusgangsdatei: %s" % (e1.get(), e2.get()))
	infile = str(e1.get())
	outfile = str(e2.get())
	master.quit()
# go_on -----------------------------------------

def main():
	global folder
	global e1
	global e2
	global root
	global f1, f2, f3, f4
	global corner1, corner2, corner3, corner4
	global scaling_matrix, container_width, container_height
	
		
	folder = e3.get()
	infile = folder+"\\"+e1.get()
	outfile = folder+"\\"+e2.get()
	
	print 'lade '+infile
	
	
	img = cv2.imread(infile, cv2.CV_LOAD_IMAGE_GRAYSCALE)
	img = scale_container_rectangular(img)
	
	#reset scaling
	corner1 = None
	corner2 = None
	corner3 = None
	corner4 = None
	f1 = False
	f2 = False
	f3 = False
	f4 = False
	scaling_matrix = None
	container_width = None
	container_height = None


	cv2.namedWindow('Binarisieren', cv2.cv.CV_WINDOW_NORMAL)	
	cv2.createTrackbar('Flaeche','Binarisieren',69,100,nothing)
	cv2.createTrackbar('Schwelle','Binarisieren',4,10,nothing)
	cv2.createTrackbar('Weichz.','Binarisieren',3,10,nothing)
	
	img_new = img.copy()
	
	print u"Der Ausschnitt wird nun binarisiert\n"
	print u"Parameter passend einstellen \nMit Leertaste bestätigen\n\n"

	while(1):
		cv2.imshow('Binarisieren',img_new)
		k = cv2.waitKey(1) & 0xFF
		if k == 27:
			cv2.destroyAllWindows()
		if k == 32:	
			break

		# get current positions of four trackbars
		f = cv2.getTrackbarPos('Flaeche','Binarisieren')
		wert = cv2.getTrackbarPos('Schwelle','Binarisieren')
		w = cv2.getTrackbarPos('Weichz.','Binarisieren')
		
		if f < 2:
			f = 2
		
		if f%2==1:
			img_new = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, f, wert)#31, 4)
		else:
			img_new = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, f+1, wert)#31, 4)
		
		
		if w%2==1:
			img_new = cv2.GaussianBlur(img_new,(w,w),0)
		else:
			img_new = cv2.GaussianBlur(img_new,(w+1,w+1),0)
	
	
	cv2.destroyAllWindows()
	
	retval = cv2.imwrite(outfile, img_new)
	print 'Schreibe in: ', outfile
	print "Aufbereitung erfolgreich!\n"

	msg = "Aufarbeitung von "+str(e1.get())+" abgeschlossen.\nErgebnis in "+str(e2.get())+" gespeichert.\n\nWähle eine neue Datei."
	showinfo(title="Aufarbeitung abgeschlossen", message=msg)
	
	return
	
	
if __name__ == "__main__":

	os.system('cls')
	print 'AKApro Version '+__version__+'\n\n'

	root = Tk()
	root.title("AKApro")
	
	Label(root, text="Ordner", padx=10, pady=13).grid(row=0, sticky=E)
	
	e3 = Entry(root)
	e3.config(width=60)
	e3.grid(row=0, column=1)

	Label(root, text="Eingangsdatei", padx=10, pady=2).grid(row=1, sticky=E)
	Label(root, text="Ausgangsdatei", padx=10, pady=2).grid(row=2, sticky=E)

	e1 = Entry(root)
	e2 = Entry(root)
	e1.config(width=60)
	e2.config(width=60)
	e1.grid(row=1, column=1, padx=10)
	e2.grid(row=2, column=1, padx=10)
	
	Button(root, text='Bild laden', command=main, padx=10).grid(row=3, column=0, columnspan=2, pady=20, padx=10)
		
	Label(root, text="\nAnleitung", font = "Times 15 bold").grid(row=5, columnspan=2)
	
	text=""" 
	Im Eingabefeld "Ordner" liegen die zu bearbeitenden Bilder ("Eingangsdatei").
	Die fertig bearbeiteten Bilder ("Ausgangsdatei") werden ebenfalls hier abgelegt, 
	sollten also anders benannt werden!
	
	ACHTUNG!   Keine Umlaute verwenden! (Im kompletten Pfad, Ordner eventuell umbenennen)
	
	Wenn das Bild geladen ist, muss zunächst der gewünschte Bildausschnitt herausgeschnitten
	werden. Dazu klickt man mit der Maus auf die linke obere Ecke des gewünschten Ausschnitts.
	Dann im Uhrzeigersinn auf die restlichen drei Ecken. (Die Reihenfolge ist wichtig!)
	Dieser Ausschnitt wird nun in ein Rechteck transfomiert. Wenn die Auswahl passt, kann 
	diese mit der Leertaste bestätigt werden. Wenn nicht, kann mit einer beliebigen anderen 
	Taste mit der Auswahl des Ausschnitts von vorn begonnen werden.
	
	ACHTUNG!   Es müssen alle vier Ecken gesetzt werden, bevor abgebrochen werden kann!
	
	Nun wird der Ausschnitt noch binarisiert, um beim Drucken Toner zu sparen. 
	Der erste Schieberegler steht für die Fläche, über die adaptiv binarisiert wird.
	Der zweite verschiebt den kalkulierten Schwellwert nach oben.
	Mit dem dritten Regler kann die Stärke des anschließenden Weichzeichners eingestellt werden.
	
	Die voreingestellten Werte funktionieren in der Regel sehr gut, aber man kann eventuell 
	noch eine bessere Einstellung für jedes Bild finden. (Einfach ausprobieren!)
	
	Durch Drücken der Leertaste wird die Aufarbeitung bestätigt und das fertige Bild in der 
	"Ausgangsdatei" gespeichert.
	
	Jetzt kann ein neues Bild aufbearbeitet werden.
	"""
	Label(root, text=text, font = "Times 10", justify=LEFT, padx=10).grid(row=6, column=0,columnspan=2)
	
	root.mainloop()

	
	
	
	
	
	
	