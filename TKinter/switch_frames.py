# -*- coding: utf-8 -*-

import Tkinter as tk   
import numpy as np
from PIL import Image, ImageTk
import cv2

TITLE_FONT = ("Helvetica", 18, "bold")

c_max_width = 500
c_max_height = 700
c_radius = 20

c_area = 31
c_thresh = 4
c_blur = 3

c_corners = np.array([[100,100],[400,100],[400,400],[100,400]])

class App(tk.Tk):

	def __init__(self, *args, **kwargs):
		tk.Tk.__init__(self, *args, **kwargs)

		# the container is where we'll stack a bunch of frames
		# on top of each other, then the one we want visible
		# will be raised above the others
		container = tk.Frame(self)
		container.pack(side="top", fill="both", expand=True)
		container.grid_rowconfigure(0, weight=1)
		container.grid_columnconfigure(0, weight=1)

		self.max_width = c_max_width
		self.max_height = c_max_height
		self.radius = c_radius
		
		self.area = c_area
		self.thresh = c_thresh
		self.blur = c_blur

		self.corners = c_corners
		
		self.moving_circle=None
		
		self.frames = {}
		
		self.frames["Laden"] = PageLaden(parent=container, controller=self)
		self.frames["Zuschneiden"] = PageZuschneiden(parent=container, controller=self)
		self.frames["Pruefen"] = PagePruefen(parent=container, controller=self)
		self.frames["Binarisieren"] = PageBinarisieren(parent=container, controller=self)

		self.frames["Laden"].grid(row=0, column=0, sticky="nsew")
		self.frames["Zuschneiden"].grid(row=0, column=0, sticky="nsew")
		self.frames["Pruefen"].grid(row=0, column=0, sticky="nsew")
		self.frames["Binarisieren"].grid(row=0, column=0, sticky="nsew")

		self.show_frame("Laden")

	def dist(self,x1,y1,x2,y2):
		return np.sqrt((x1-x2)**2+(y1-y2)**2)
		
	def load_image(self):
		self.img_raw = cv2.imread('1.jpg', cv2.CV_LOAD_IMAGE_GRAYSCALE)		#save img_raw for transformation (just in background)
		img_temp = Image.fromarray(self.img_raw)															# use PIL image for displaying
		
		img_width, img_height = img_temp.size
		aspect_ratio = float(img_width)/img_height		#save aspect ratio
		
		if img_height > self.max_height:		#image must not be higher than max height
			img_temp = img_temp.resize((int(aspect_ratio*self.max_height), self.max_height), Image.ANTIALIAS)	#resizing
			img_width, img_height = img_temp.size		#get new size
			
		if img_width > self.max_width:			# same for width
			img_temp = img_temp.resize((self.max_width, int(self.max_width/aspect_ratio)), Image.ANTIALIAS)
			img_width, img_height = img_temp.size
		
		self.img_display_width = img_width
		self.img_display_height = img_height
		
		# === TODO === Startecken sinnvoll setzen 
		
		self.img_display=ImageTk.PhotoImage(img_temp)		#for display PhotoImage is needed
		self.frames['Zuschneiden'].display.create_image(0,0, anchor = 'nw', image=self.img_display)			#show PhotoImage in Canvas
		
		
		c = self.corners		#nur Abkürzung
		f = self.frames['Zuschneiden']
		
		f.circle_list = []
		f.line_list = []
		
		radius = self.radius
		f.circle_list.append(f.display.create_oval(c[0,0]-radius, c[0,1]-radius,c[0,0]+radius, c[0,1]+radius))
		f.circle_list.append(f.display.create_oval(c[1,0]-radius, c[1,1]-radius, c[1,0]+radius, c[1,1]+radius))
		f.circle_list.append(f.display.create_oval(c[2,0]-radius, c[2,1]-radius, c[2,0]+radius, c[2,1]+radius))
		f.circle_list.append(f.display.create_oval(c[3,0]-radius, c[3,1]-radius, c[3,0]+radius, c[3,1]+radius))
		
		f.line_list.append(f.display.create_line(c[0,0],c[0,1],c[1,0],c[1,1], fill='green', width=3))
		f.line_list.append(f.display.create_line(c[1,0],c[1,1],c[2,0],c[2,1], fill='green', width=3))
		f.line_list.append(f.display.create_line(c[2,0],c[2,1],c[3,0],c[3,1], fill='green', width=3))
		f.line_list.append(f.display.create_line(c[3,0],c[3,1],c[0,0],c[0,1], fill='green', width=3))
		
		self.show_frame('Zuschneiden')
					
	def set_moving_circle(self, event):
		f = self.frames['Zuschneiden']
		for c in f.circle_list:
			[x1,y1,x2,y2] = f.display.coords(c)
			if self.dist(0.5*(x1+x2), 0.5*(y1+y2), event.x, event.y) <= self.radius:
				print 'set moving circle', f.circle_list.index(c), 'coords:', event.x, event.y
				self.moving_circle = c
				break
				
	def reset_moving_circle(self, event):
		if self.moving_circle != None:
			self.moving_circle = None
			print '\nreset moving circle'
			
	def move(self, event):
		f = self.frames['Zuschneiden']
		if event.x <0 or event.x > self.img_display_width or event.y <0 or event.y > self.img_display_height:		#outside of display -> do not move
			return
			
		if self.moving_circle != None:
			i = f.circle_list.index(self.moving_circle)
			print '\rmove',
			
			f.display.coords(self.moving_circle, event.x-self.radius, event.y-self.radius, event.x+self.radius, event.y+self.radius)	#move circle
			
			if i==0:
				old = f.display.coords(f.line_list[3])			# moving first line
				new = [old[0], old[1], event.x, event.y]
				f.display.coords(f.line_list[3], *new)
				old = f.display.coords(f.line_list[0])				#moving second line
				new = [event.x, event.y, old[2], old[3]]
				f.display.coords(f.line_list[0], *new)
			if i==1:
				old = f.display.coords(f.line_list[0])			# moving first line
				new = [old[0], old[1], event.x, event.y]
				f.display.coords(f.line_list[0], *new)
				old = f.display.coords(f.line_list[1])				#moving second line
				new = [event.x, event.y, old[2], old[3]]
				f.display.coords(f.line_list[1], *new)
			if i==2:
				old = f.display.coords(f.line_list[1])			# moving first line
				new = [old[0], old[1], event.x, event.y]
				f.display.coords(f.line_list[1], *new)
				old = f.display.coords(f.line_list[2])				#moving second line
				new = [event.x, event.y, old[2], old[3]]
				f.display.coords(f.line_list[2], *new)
			if i==3:
				old = f.display.coords(f.line_list[2])			# moving first line
				new = [old[0], old[1], event.x, event.y]
				f.display.coords(f.line_list[2], *new)
				old = f.display.coords(f.line_list[3])				#moving second line
				new = [event.x, event.y, old[2], old[3]]
				f.display.coords(f.line_list[3], *new)		
		
	def save_coords(self):
		f = self.frames['Zuschneiden']
		for i in [0,1,2,3]:
			[x1,y1,x2,y2]=f.display.coords(f.circle_list[i])
			self.corners[i] = np.array([(x1+x2)/2, (y1+y2)/2])
		
	def transform(self):
		self.save_coords() # save coords of corners in self.corners
		
		(height_temp, width_temp)  = self.img_raw.shape		# Die Koordinaten von PhotoImage auf array übertragen
		self.corners_raw = np.float32(np.copy(self.corners))
		for c in self.corners_raw:
			c[0] = c[0]/self.img_display_width*width_temp
			c[1] = c[1]/self.img_display_height*height_temp

		#transform
		pts1 = np.float32([self.corners_raw[0], self.corners_raw[1], self.corners_raw[3], self.corners_raw[2]])						#'alte' Koordinaten der Ecken, Reihenfolge für Trafo: lo ro lu ru = 0 1 3 2
		width = int((pts1[1,0]+pts1[3,0]-pts1[0,0]-pts1[2,0])/2)		#Mittlere Breite und
		height = int((pts1[3,1]+pts1[2,1]-pts1[0,1]-pts1[1,1])/2)	#Mittlere Höhe des Bildausschnitts
		pts2 = np.float32([[0,0],[width,0],[0,height],[width,height]])		#'neue' Koordinaten der Ecken = Rechteck
		scaling_matrix = cv2.getPerspectiveTransform(pts1,pts2)									# pts1 is the original scaling, pts2 is the scaling you want to have
		self.img_cut_raw = cv2.warpPerspective(self.img_raw, scaling_matrix, (width, height))		# das transformierte Bild speichern


		
		#show cut image in frame Pruefen
		img_temp = Image.fromarray(self.img_cut_raw)						# use PIL image for displaying
		
		img_width, img_height = img_temp.size
		aspect_ratio = float(img_width)/img_height		#save aspect ratio
		
		if img_height > self.max_height:		#image must not be higher than max height
			img_temp = img_temp.resize((int(aspect_ratio*self.max_height), self.max_height), Image.ANTIALIAS)	#resizing
			img_width, img_height = img_temp.size		#get new size
			
		if img_width > self.max_width:			# same for width
			img_temp = img_temp.resize((self.max_width, int(self.max_width/aspect_ratio)), Image.ANTIALIAS)
			img_width, img_height = img_temp.size

		self.img_cut_display=ImageTk.PhotoImage(img_temp)		#for display PhotoImage is needed
		
		self.frames['Pruefen'].display.create_image(0,0, anchor = 'nw', image=self.img_cut_display)			#show PhotoImage in Canvas
		
		self.show_frame('Pruefen')
	
	def save_cut(self):
		cv2.imwrite('1a.jpg', self.img_cut_raw)
		self.cancel()
	
	def first_binarise(self):
		f = self.frames['Binarisieren']
		self.img_bin = np.copy(self.img_cut_raw)
		img_temp = Image.fromarray(self.img_bin)						# use PIL image for displaying
		self.img_bin_display=ImageTk.PhotoImage(img_temp)		#for display PhotoImage is needed
		f.imobj = f.display.create_image(0,0, anchor = 'nw', image=self.img_bin_display)			#show PhotoImage in Canvas
		f.slider1.configure(command=f.sl1)		#enable binarization (if done at init, it will be used then and there ist not jet an img_cut_raw)
		f.slider2.configure(command=f.sl2)		#enable binarization (if done at init, it will be used then and there ist not jet an img_cut_raw)
		f.slider3.configure(command=f.sl3)		#enable binarization (if done at init, it will be used then and there ist not jet an img_cut_raw)
		self.binarise()				#first binarise
		self.show_frame('Binarisieren')
	
	def binarise(self):
		self.img_bin = cv2.adaptiveThreshold(self.img_cut_raw, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, self.area, self.thresh)
		self.img_bin = cv2.GaussianBlur(self.img_bin,(self.blur,self.blur),0)
		
		#show binarised image in frame Binarisieren
		img_temp = Image.fromarray(self.img_bin)						# use PIL image for displaying
		
		img_width, img_height = img_temp.size
		aspect_ratio = float(img_width)/img_height		#save aspect ratio
		
		if img_height > self.max_height:		#image must not be higher than max height
			img_temp = img_temp.resize((int(aspect_ratio*self.max_height), self.max_height), Image.ANTIALIAS)	#resizing
			img_width, img_height = img_temp.size		#get new size
			
		if img_width > self.max_width:			# same for width
			img_temp = img_temp.resize((self.max_width, int(self.max_width/aspect_ratio)), Image.ANTIALIAS)
			img_width, img_height = img_temp.size

		self.img_bin_display=ImageTk.PhotoImage(img_temp)		#for display PhotoImage is needed
		self.frames['Binarisieren'].display.itemconfig(self.frames['Binarisieren'].imobj, image=self.img_bin_display)		#update image
		
	def save_bin(self):
		cv2.imwrite('1a.jpg', self.img_bin)
		self.cancel()
	
	def cancel(self):
		del self.img_raw
		del self.img_display
		del self.img_display_width
		del self.img_display_height
		del self.frames['Zuschneiden'].circle_list
		del self.frames['Zuschneiden'].line_list
		del self.img_cut_raw
		del self.img_cut_display
		del self.img_bin
		del self.img_bin_display
		del self.area 
		del self.thresh
		del self.blur
		del self.corners 
		self.show_frame('Laden')
		
	def show_frame(self, page_name):
		'''Show a frame for the given page name'''
		frame = self.frames[page_name]
		frame.tkraise()



class PageLaden(tk.Frame):

	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		self.controller = controller
		label = tk.Label(self, text="Bild laden", font=TITLE_FONT)
		label.pack(side="top", fill="x", pady=10)

		button1 = tk.Button(self, text="Laden", command=self.controller.load_image)
		button1.pack()

		
class PageZuschneiden(tk.Frame):

	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		self.controller = controller
		
		label = tk.Label(self, text="Zuschneiden", font=TITLE_FONT)
		label.pack(side="top", fill="x", pady=10)
		
		button1 = tk.Button(self, text="Zuschneiden", command=self.controller.transform)
		button1.pack()
		button2 = tk.Button(self, text="Abbrechen", command=self.controller.cancel)
		button2.pack()
		
		#display image		
		self.display = tk.Canvas(self, width=self.controller.max_width, height=self.controller.max_height)
		self.display.bind('<Button-1>', self.controller.set_moving_circle)
		self.display.bind('<ButtonRelease-1>', self.controller.reset_moving_circle)
		self.display.bind('<B1-Motion>', self.controller.move)
		self.display.pack()


class PagePruefen(tk.Frame):

	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		self.controller = controller
		label = tk.Label(self, text=u'Überprüfen', font=TITLE_FONT)
		label.pack(side="top", fill="x", pady=10)
		button1 = tk.Button(self, text="Passt", command=self.controller.first_binarise)
		button1.pack()
		button2 = tk.Button(self, text="Korrigieren", command=lambda: controller.show_frame("Zuschneiden"))
		button2.pack()
		button3 = tk.Button(self, text="Abbrechen", command=self.controller.cancel)
		button3.pack()
		
		self.display = tk.Canvas(self, width=self.controller.max_width, height=self.controller.max_height)
		self.display.pack()

		
class PageBinarisieren(tk.Frame):

	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		self.controller = controller
		label = tk.Label(self, text="Binarisieren", font=TITLE_FONT)
		label.pack(side="top", fill="x", pady=10)
		
		button1 = tk.Button(self, text="Speichern", command=self.controller.save_bin)
		button1.pack()
		button2 = tk.Button(self, text="Abbrechen", command=self.controller.cancel)
		button2.pack()
		
		self.slider1 = tk.Scale(self, from_=2, to=50, resolution=2, orient='horizontal')
		self.slider1.set(self.controller.area-1)
		self.slider1.pack()
		self.slider2 = tk.Scale(self, from_=0, to=10, resolutio=1, orient='horizontal')
		self.slider2.set(self.controller.thresh)
		self.slider2.pack()
		self.slider3 = tk.Scale(self, from_=2, to=14, resolution=2, orient='horizontal')
		self.slider3.set(self.controller.blur-1)
		self.slider3.pack()
		
		self.display = tk.Canvas(self, width=self.controller.max_width, height=self.controller.max_height)
		self.display.pack()
		
	def sl1(self,area):
		self.controller.area = int(area)+1	#bug in tkinter, not possible to get just odd values
		self.controller.binarise()
		
	def sl2(self,thresh):
		self.controller.thresh = int(thresh)
		self.controller.binarise()
		
	def sl3(self,blur):
		self.controller.blur = int(blur)+1
		self.controller.binarise()
		
		
if __name__ == "__main__":
	app = App()
	app.mainloop()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	