# -*- coding: utf-8 -*-

import Tkinter as tk
import numpy as np
from PIL import Image, ImageTk
import cv2

root = tk.Tk()
arr = cv2.imread('1.jpg', cv2.CV_LOAD_IMAGE_GRAYSCALE)
#arr=np.ones([256,256])
img=Image.fromarray(arr)
imgTk=ImageTk.PhotoImage(img)

display = tk.Label(root, image=imgTk)
display.pack()
root.mainloop()