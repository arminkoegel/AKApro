# -*- coding: utf-8 -*-

import Tkinter as tk
from PIL import Image, ImageTk
import numpy as np
import cv2

total_height = 700
filename = '1.jpg'
#filename = '1.gif'

#img = cv2.imread(filename, cv2.CV_LOAD_IMAGE_GRAYSCALE)


root = tk.Tk()

frame = tk.Frame(root)
frame.pack()

img_load = Image.open(filename)
img_width, img_height = img_load.size
aspect_ratio = float(img_width)/img_height
img_load = img_load.resize((int(aspect_ratio*total_height), total_height), Image.ANTIALIAS)
img = ImageTk.PhotoImage(img_load)

#img = ImageTk.PhotoImage(Image.open(filename).resize((300,500), Image.ANTIALIAS))


#lab = tk.Label(frame, image=img)
#lab.pack()
display = tk.Canvas(frame, width=int(aspect_ratio*total_height), height=total_height)
display.create_image(0,0, anchor = 'nw', image=img)
display.pack()


root.mainloop()