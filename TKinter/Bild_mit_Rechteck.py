# -*- coding: utf-8 -*-

import Tkinter as tk
import numpy as np
from PIL import Image, ImageTk
import cv2

total_height = 800
filename = '1.jpg'

radius = 20		# sollte gerade sein (Integer-Teilung, blabla...)
moving_circle = None

# main window
root = tk.Tk()
frame = tk.Frame(root, width=500, height=total_height+6, bd=3, relief=tk.RIDGE)
frame.pack_propagate(0) # don't shrink
frame.pack(side=tk.LEFT)



#loading image
frame.img_load = Image.open(filename)		# im Objekt frame speichern, damit nichts gelöscht wird. Hat nichts mit der Darstellung zu tun, könnte auch ein anders Objekt sein
img_width, img_height = frame.img_load.size
aspect_ratio = float(img_width)/img_height
total_width = int(aspect_ratio*total_height)
frame.img_load = frame.img_load.resize((int(aspect_ratio*total_height), total_height), Image.ANTIALIAS)
img = ImageTk.PhotoImage(frame.img_load)


def dist(x1,y1,x2,y2):
	return np.sqrt((x1-x2)**2+(y1-y2)**2)

def click(event):
	print 'new circle', event.x, event.y
	circ = display.create_oval(event.x-radius, event.y-radius, event.x+radius, event.y+radius)
	circle_list.append(circ)
	 
def delete(event):
	del_list = []
	for c in circle_list:
		[x1,y1,x2,y2] = display.coords(c)

		if dist(0.5*(x1+x2), 0.5*(y1+y2), event.x, event.y) <= radius:
			print 'deleting circle', event.x, event.y
			del_list.append(c)
			display.delete(c)
	for c in del_list:
		circle_list.remove(c)

def move(event):
	global moving_circle
	if event.x <0 or event.x >int(aspect_ratio*total_height) or event.y <0 or event.y >total_height:		#outside of display
		moving_circle=None
		print '\rreset moving circle'

		
	if moving_circle != None:
		i = circle_list.index(moving_circle)
		print '\rmove',
		display.coords(moving_circle, event.x-radius, event.y-radius, event.x+radius, event.y+radius)
		if i==0:
			old = display.coords(line_list[3])			# moving first line
			new = [old[0], old[1], event.x, event.y]
			display.coords(line_list[3], *new)
			old = display.coords(line_list[0])				#moving second line
			new = [event.x, event.y, old[2], old[3]]
			display.coords(line_list[0], *new)
		if i==1:
			old = display.coords(line_list[0])			# moving first line
			new = [old[0], old[1], event.x, event.y]
			display.coords(line_list[0], *new)
			old = display.coords(line_list[1])				#moving second line
			new = [event.x, event.y, old[2], old[3]]
			display.coords(line_list[1], *new)
		if i==2:
			old = display.coords(line_list[1])			# moving first line
			new = [old[0], old[1], event.x, event.y]
			display.coords(line_list[1], *new)
			old = display.coords(line_list[2])				#moving second line
			new = [event.x, event.y, old[2], old[3]]
			display.coords(line_list[2], *new)
		if i==3:
			old = display.coords(line_list[2])			# moving first line
			new = [old[0], old[1], event.x, event.y]
			display.coords(line_list[2], *new)
			old = display.coords(line_list[3])				#moving second line
			new = [event.x, event.y, old[2], old[3]]
			display.coords(line_list[3], *new)
			
def set_moving_circle(event):
	global moving_circle
	for c in circle_list:
		[x1,y1,x2,y2] = display.coords(c)
		if dist(0.5*(x1+x2), 0.5*(y1+y2), event.x, event.y) <= radius:
			print 'set moving circle', circle_list.index(c), 'coords:', event.x, event.y
			moving_circle = c
			break

def reset_moving_circle(event):
	global moving_circle
	if moving_circle != None:
		moving_circle = None
		print '\rreset moving circle'

def get_coords():
	corners = []
	for i in [0,1,2,3]:
		[x1,y1,x2,y2]=display.coords(circle_list[i])
		corners.append([(float(x1)+x2)/2, (float(y1)+y2)/2])
	return corners

def transform(img, corners):
		pts1 = np.float32([corners[0], corners[1], corners[3], corners[2]])						#'alte' Koordinaten der Ecken, Reihenfolge für Trafo: lo ro lu ru = 0 1 3 2
		width = int((pts1[1,0]+pts1[3,0]-pts1[0,0]-pts1[2,0])/2)	#Mittlere Breite und
		height = int((pts1[3,1]+pts1[2,1]-pts1[0,1]-pts1[1,1])/2)	#Mittlere Höhe des Bildausschnitts
		pts2 = np.float32([[0,0],[width,0],[0,height],[width,height]])		#'neue' Koordinaten der Ecken = Rechteck
		scaling_matrix = cv2.getPerspectiveTransform(pts1,pts2)									# pts1 is the original scaling, pts2 is the scaling you want to have
		return cv2.warpPerspective(img, scaling_matrix, (width, height))		# das transformierte Bild

def do_transformation():
		img_temp = cv2.imread(filename, cv2.CV_LOAD_IMAGE_GRAYSCALE)
		(height_temp, width_temp)  = img_temp.shape
		coords_temp = get_coords()
		for c in coords_temp:
			c[0] = c[0]/total_width*width_temp
			c[1] = c[1]/total_height*height_temp
		img_t = transform(img_temp, coords_temp)
		cv2.imwrite('temp.bmp', img_t)

#display frame around image		
dframe = tk.Frame(root, width=int(aspect_ratio*total_height), height=total_height)
dframe.pack(side=tk.LEFT)
display = tk.Canvas(dframe, width=int(aspect_ratio*total_height), height=total_height)


display.create_image(0,0, anchor = 'nw', image=img)

circle_list = []
circle_list.append(display.create_oval(100-radius, 100-radius, 100+radius, 100+radius))
circle_list.append(display.create_oval(400-radius, 100-radius, 400+radius, 100+radius))
circle_list.append(display.create_oval(400-radius, 400-radius, 400+radius, 400+radius))
circle_list.append(display.create_oval(100-radius, 400-radius, 100+radius, 400+radius))

line_list = []
line_list.append(display.create_line(100,100,400,100, fill='green', width=3))
line_list.append(display.create_line(400,100,400,400, fill='green', width=3))
line_list.append(display.create_line(400,400,100,400, fill='green', width=3))
line_list.append(display.create_line(100,400,100,100, fill='green', width=3))


display.bind('<Button-1>', set_moving_circle)
display.bind('<ButtonRelease-1>', reset_moving_circle)
display.bind('<B1-Motion>', move)
display.pack()

B_get_coords = tk.Button(frame, text='OK', command=do_transformation)
B_get_coords.pack()


root.mainloop()
