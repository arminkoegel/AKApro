# -*- coding: utf-8 -*-

import Tkinter as tk
import numpy as np

radius = 10

circle_list = []
moving_circle = None

def dist(x1,y1,x2,y2):
	return np.sqrt((x1-x2)**2+(y1-y2)**2)

def create(event):
	print 'new circle', event.x, event.y
	circ = display.create_oval(event.x-radius, event.y-radius, event.x+radius, event.y+radius)
	circle_list.append(circ)
	 
	 
def delete(event):
	del_list = []
	for c in circle_list:
		[x1,y1,x2,y2] = display.coords(c)

		if dist(0.5*(x1+x2), 0.5*(y1+y2), event.x, event.y) <= radius:
			print 'deleting circle', event.x, event.y
			del_list.append(c)
			display.delete(c)
	for c in del_list:
		circle_list.remove(c)

def move(event):
	if moving_circle != None:
		print '\rmove',
		display.coords(moving_circle, event.x-radius, event.y-radius, event.x+radius, event.y+radius)
			
def set_moving_circle(event):
	global moving_circle
	for c in circle_list:
		[x1,y1,x2,y2] = display.coords(c)
		if dist(0.5*(x1+x2), 0.5*(y1+y2), event.x, event.y) <= radius:
			print 'set moving circle', event.x, event.y
			moving_circle = c
			break

def reset_moving_circle(event):
	global moving_circle
	if moving_circle != None:
		moving_circle = None
		print '\nreset moving circle'



		
root = tk.Tk()

frame = tk.Frame(root)
frame.pack()

display = tk.Canvas(frame, width=500, height=500)
display.bind('<Double-Button-1>', create)
display.bind('<Button-1>', set_moving_circle)
display.bind('<ButtonRelease-1>', reset_moving_circle)
display.bind('<Button-3>', delete)
display.bind('<B1-Motion>', move)
display.pack()


root.mainloop()
