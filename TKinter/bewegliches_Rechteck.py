# -*- coding: utf-8 -*-

import Tkinter as tk
import numpy as np

radius = 10

moving_circle = None

def dist(x1,y1,x2,y2):
	return np.sqrt((x1-x2)**2+(y1-y2)**2)

def click(event):
	print 'new circle', event.x, event.y
	circ = display.create_oval(event.x-radius, event.y-radius, event.x+radius, event.y+radius)
	circle_list.append(circ)
	 
	 
def delete(event):
	del_list = []
	for c in circle_list:
		[x1,y1,x2,y2] = display.coords(c)

		if dist(0.5*(x1+x2), 0.5*(y1+y2), event.x, event.y) <= radius:
			print 'deleting circle', event.x, event.y
			del_list.append(c)
			display.delete(c)
	for c in del_list:
		circle_list.remove(c)

def move(event):
	global moving_circle
	if event.x <0 or event.x >500 or event.y <0 or event.y >500:		#outside of display
		moving_circle=None
		print '\nreset moving circle'

		
	if moving_circle != None:
		i = circle_list.index(moving_circle)
		print '\rmove',
		display.coords(moving_circle, event.x-radius, event.y-radius, event.x+radius, event.y+radius)
		if i==0:
			old = display.coords(line_list[3])			# moving first line
			new = [old[0], old[1], event.x, event.y]
			display.coords(line_list[3], *new)
			old = display.coords(line_list[0])				#moving second line
			new = [event.x, event.y, old[2], old[3]]
			display.coords(line_list[0], *new)
		if i==1:
			old = display.coords(line_list[0])			# moving first line
			new = [old[0], old[1], event.x, event.y]
			display.coords(line_list[0], *new)
			old = display.coords(line_list[1])				#moving second line
			new = [event.x, event.y, old[2], old[3]]
			display.coords(line_list[1], *new)
		if i==2:
			old = display.coords(line_list[1])			# moving first line
			new = [old[0], old[1], event.x, event.y]
			display.coords(line_list[1], *new)
			old = display.coords(line_list[2])				#moving second line
			new = [event.x, event.y, old[2], old[3]]
			display.coords(line_list[2], *new)
		if i==3:
			old = display.coords(line_list[2])			# moving first line
			new = [old[0], old[1], event.x, event.y]
			display.coords(line_list[2], *new)
			old = display.coords(line_list[3])				#moving second line
			new = [event.x, event.y, old[2], old[3]]
			display.coords(line_list[3], *new)
			
def set_moving_circle(event):
	global moving_circle
	for c in circle_list:
		[x1,y1,x2,y2] = display.coords(c)
		if dist(0.5*(x1+x2), 0.5*(y1+y2), event.x, event.y) <= radius:
			print 'set moving circle', event.x, event.y
			moving_circle = c
			break

def reset_moving_circle(event):
	global moving_circle
	if moving_circle != None:
		moving_circle = None
		print '\nreset moving circle'



		
root = tk.Tk()

frame = tk.Frame(root)
frame.pack()

display = tk.Canvas(frame, width=500, height=500)

circle_list = []
circle_list.append(display.create_oval(100-radius, 100-radius, 100+radius, 100+radius))
circle_list.append(display.create_oval(400-radius, 100-radius, 400+radius, 100+radius))
circle_list.append(display.create_oval(400-radius, 400-radius, 400+radius, 400+radius))
circle_list.append(display.create_oval(100-radius, 400-radius, 100+radius, 400+radius))

line_list = []
line_list.append(display.create_line(100,100,400,100))
line_list.append(display.create_line(400,100,400,400))
line_list.append(display.create_line(400,400,100,400))
line_list.append(display.create_line(100,400,100,100))


display.bind('<Button-1>', set_moving_circle)
display.bind('<ButtonRelease-1>', reset_moving_circle)
display.bind('<B1-Motion>', move)

display.pack()


root.mainloop()
