# -*- coding: utf-8 -*-

__author__	= u'Armin Kögel'
__contact__	= u'arminkoegel@hotmail.de'
__version__	= '1.3'

"""
Getestet unter Windows 7 und 8 mit
Python 2.7.9
OpenCV 2.4.11
Numpy  1.9.2

Verbesserungen:
* Speicherplatzbereinigung wird nach jedem Bild durchgeführt
"""

print 'lade...'

import os
import sys
import copy
import cv2
import numpy as np
from Tkinter import *
from tkMessageBox import *

#global variables
corner1 = None
corner2 = None
corner3 = None
corner4 = None

scaling_matrix = None
container_width = None
container_height = None

foler = None
infile = None
outfile = None

SAVE_RAW = False
EXIT = False

# scale_container_rectangular ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def scale_container_rectangular(img):
	global scaling_matrix
	
	set_scaling_parameters(img)

	return cv2.warpPerspective(img, scaling_matrix, (container_width, container_height)) # here the transformation is done
# scale_container_rectangular --------------------------------------------------------

# set_corner ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def set_corners(event, x, y, flags, params):
	global corner1, corner2, corner3, corner4
	
	if event == cv2.EVENT_LBUTTONDOWN:# and corner4 != None:
		if corner1 == None:
			corner1 = [x,y]
		elif corner2 == None:
			corner2 = [x,y]
		elif corner3 == None:
			corner3 = [x,y]
		elif corner4 == None:
			corner4 = [x,y]
		
		cv2.destroyAllWindows()
			
	if event == cv2.EVENT_RBUTTONDOWN:# and corner1 != None:
		if corner4 != None:
			corner4 = None
		elif corner3 != None:
			corner3 = None
		elif corner2 != None:
			corner2 = None
		elif corner1 != None:
			corner1 = None
			
		cv2.destroyAllWindows()


# set_corner --------------------------------------------------------
		
# set_scaling_parameters ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def set_scaling_parameters(img):
	global corner1, corner2, corner3, corner4
	global scaling_matrix, container_width, container_height
	global SAVE_RAW, EXIT
	
	print u"\nDer gewünschte Ausschnitt wird in ein Rechteck transformiert."

	img_temp = copy.copy(img)

	while True:

		print u"\nAuf die Ecken des gewünschten Ausschnitt klicken."
		print u"Beginne links oben, weiter im Uhrzeigersinn!"
		print u"Mit Rechtsklick wird der letzte Punkt wieder entfernt"
		print u"Mit Leertaste bestätigen"
		while True:
		
			cv2.namedWindow('Transformieren', cv2.WINDOW_NORMAL)	# show the whole image
			cv2.imshow('Transformieren', img_temp)
			
			cv2.setMouseCallback('Transformieren',set_corners)		# if there is a mouse callback, call set_corners

			img_temp = copy.copy(img)
			if corner1 != None:
				cv2.circle(img_temp,(corner1[0],corner1[1]), 20, 255, -1)
			if corner2 != None:
				cv2.circle(img_temp,(corner2[0],corner2[1]), 20, 255, -1)
				cv2.line(img_temp,(corner1[0],corner1[1]),(corner2[0],corner2[1]),255,5)
			if corner3 != None:
				cv2.circle(img_temp,(corner3[0],corner3[1]), 20, 255, -1)
				cv2.line(img_temp,(corner2[0],corner2[1]),(corner3[0],corner3[1]),255,5)
			if corner4 != None:
				cv2.circle(img_temp,(corner4[0],corner4[1]), 20, 255, -1)
				cv2.line(img_temp,(corner3[0],corner3[1]),(corner4[0],corner4[1]),255,5)
				cv2.line(img_temp,(corner4[0],corner4[1]),(corner1[0],corner1[1]),255,5)
				
			cv2.namedWindow('Transformieren', cv2.WINDOW_NORMAL)					# show the whole image
			cv2.imshow('Transformieren', img_temp)

			ans = cv2.waitKey(0)
			if ans == 32 and corner4 != None: 
				cv2.destroyAllWindows()
				break
			


		pts1 = np.float32([corner1, corner2, corner4, corner3])						#corner4 comes before corner3 for clockwise sequence
		container_width = (corner2[0]+corner3[0])/2 - (corner1[0]+corner4[0])/2		#get the mean width and height of the container
		container_height = (corner4[1]+corner3[1])/2 - (corner1[1]+corner2[1])/2
		pts2 = np.float32([[0,0],[container_width,0],[0,container_height],[container_width,container_height]])
		scaling_matrix = cv2.getPerspectiveTransform(pts1,pts2)									# pts1 is the original scaling, pts2 is the scaling you want to have
		dst = cv2.warpPerspective(img,scaling_matrix,(container_width, container_height))

		print u"\nAusschnittbreite (px): ", container_width, " , Ausschnitthoehe (px): ", container_height

		print u"\n\nPasst das Ergebnis?"
		print u"Bestätigen und Binarisieren: Leertaste"
		print u"Bestätigen und direkt speichern: Enter"
		print u"Abbrechen: Escape"
		print u"Wiederholung: Beliebige Taste\n"

		cv2.namedWindow('Transformieren', cv2.WINDOW_NORMAL)
		cv2.imshow('Transformieren', dst)
		ans = cv2.waitKey(0)							# wait for pressing space. If anything else is pressed, stay in while-loop
		cv2.destroyAllWindows()

			
		if ans == 32:			# 32 is the code for space. The while-loop stops when space is pressed
			scaling_matrix = scaling_matrix			# set the scaling parameters in global_parameters
			container_height = container_height
			container_width = container_width
			print u"Bildausschnitt transformiert!\n\n"
			break
			
		if ans == 13:			
			scaling_matrix = scaling_matrix			
			container_height = container_height
			container_width = container_width
			print u"Bildausschnitt transformiert!\n\n"
			print u"Bild wird nicht binarisiert\n"
			SAVE_RAW = True
			break
		
		if ans == 27:			
			scaling_matrix = scaling_matrix			
			container_height = container_height
			container_width = container_width
			print u"\n\nABBRUCH\n\n\nNeues Bild laden"
			SAVE_RAW = True
			EXIT = True
			break
		
		img_temp = copy.copy(img)
		cv2.circle(img_temp,(corner1[0],corner1[1]), 20, 255, 3)
		cv2.circle(img_temp,(corner2[0],corner2[1]), 20, 255, 3)
		cv2.circle(img_temp,(corner3[0],corner3[1]), 20, 255, 3)
		cv2.circle(img_temp,(corner4[0],corner4[1]), 20, 255, 3)
		
# set_scaling_parameters ----------------------------------------------------------

def nothing(input):
	pass
	
# go_on +++++++++++++++++++++++++++++++++++++++++	
def go_on():
	global infile
	global outfile
	global e1
	global e2
	global master
	print(u"\nEingangsdatei: %s\nAusgangsdatei: %s" % (e1.get(), e2.get()))
	infile = str(e1.get())
	outfile = str(e2.get())
	master.quit()
# go_on -----------------------------------------

def main():
	global folder
	global e1
	global e2
	global root
	global f1, f2, f3, f4
	global corner1, corner2, corner3, corner4
	global scaling_matrix, container_width, container_height
	global SAVE_RAW, EXIT
		
	folder = e3.get()
	infile = folder+"\\"+e1.get()
	outfile = folder+"\\"+e2.get()
	
	
	print 'lade '+infile
	
	
	img = cv2.imread(infile, cv2.CV_LOAD_IMAGE_GRAYSCALE)
	img = scale_container_rectangular(img)

	#reset scaling params
	corner1 = None
	corner2 = None
	corner3 = None
	corner4 = None
	f1 = False
	f2 = False
	f3 = False
	f4 = False
	scaling_matrix = None
	container_width = None
	container_height = None
	
	img_new = img.copy()
	print 'Speicherplatz 1: ', id(img)
	print 'Speicherplatz 2: ', id(img_new)
	if SAVE_RAW == False:
		cv2.namedWindow('Binarisieren', cv2.cv.CV_WINDOW_NORMAL)	
		cv2.createTrackbar('Flaeche','Binarisieren',69,100,nothing)
		cv2.createTrackbar('Schwelle','Binarisieren',4,10,nothing)
		cv2.createTrackbar('Weichz.','Binarisieren',3,10,nothing)	
		
	
		
		print u"Der Ausschnitt wird nun binarisiert\n"
		print u"Parameter passend einstellen"
		print u"Speichern: Enter"
		print u"Abbrechen: Escape\n\n"

		while(1):
			cv2.imshow('Binarisieren',img_new)
			k = cv2.waitKey(1) & 0xFF
			if k == 13:	
				break
			if k == 27:
				EXIT = True
				print u"\n\nABBRUCH\n\n\nNeues Bild laden"
				break
				
			# get current positions of four trackbars
			f = cv2.getTrackbarPos('Flaeche','Binarisieren')
			wert = cv2.getTrackbarPos('Schwelle','Binarisieren')
			w = cv2.getTrackbarPos('Weichz.','Binarisieren')
			
			if f < 2:
				f = 2
			
			if f%2==1:
				img_new = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, f, wert)#31, 4)
			else:
				img_new = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, f+1, wert)#31, 4)	
			
			if w%2==1:
				img_new = cv2.GaussianBlur(img_new,(w,w),0)
			else:
				img_new = cv2.GaussianBlur(img_new,(w+1,w+1),0)
				
		cv2.destroyAllWindows()
	
	if EXIT == False:
		retval = cv2.imwrite(outfile, img_new)
		print 'Schreibe in: ', outfile
		print "Aufbereitung erfolgreich!\n"

		msg = "Aufarbeitung von "+str(e1.get())+" abgeschlossen.\nErgebnis in "+str(e2.get())+" gespeichert.\n\nWähle eine neue Datei."
		showinfo(title="Aufarbeitung abgeschlossen", message=msg)
		
	SAVE_RAW = False
	EXIT = False
	
	del(img)		#Speicherplatzbereinigung
	del(img_new)
	
	return
	
	
if __name__ == "__main__":

	os.system('cls')
	print 'AKApro Version '+__version__+'\n\n'

	root = Tk()
	root.title("AKApro")
	
	Label(root, text="Ordner", padx=10, pady=13).grid(row=0, sticky=E)
	
	e3 = Entry(root)
	e3.config(width=60)
	e3.grid(row=0, column=1)

	Label(root, text="Eingangsdatei", padx=10, pady=2).grid(row=1, sticky=E)
	Label(root, text="Ausgangsdatei", padx=10, pady=2).grid(row=2, sticky=E)

	e1 = Entry(root)
	e2 = Entry(root)
	e1.config(width=60)
	e2.config(width=60)
	e1.grid(row=1, column=1, padx=10)
	e2.grid(row=2, column=1, padx=10)
	
	Button(root, text='Bild laden', command=main, padx=10).grid(row=3, column=0, columnspan=2, pady=20, padx=10)
	
	Label(root, text="\nAnleitung", font = "Times 15 bold").grid(row=5, columnspan=2)
	
	text=""" 
	Im Eingabefeld "Ordner" liegen die zu bearbeitenden Bilder ("Eingangsdatei").
	Die fertig bearbeiteten Bilder ("Ausgangsdatei") werden ebenfalls hier abgelegt, 
	sollten also anders benannt werden!
	
	ACHTUNG!   Keine Umlaute verwenden! (Im kompletten Pfad, Ordner eventuell umbenennen)
	
	Wenn das Bild geladen ist, muss zunächst der gewünschte Bildausschnitt herausgeschnitten
	werden. Dazu klickt man mit der linken Maustaste auf die linke obere Ecke des gewünschten Ausschnitts.
	Dann im Uhrzeigersinn auf die restlichen drei Ecken. (Die Reihenfolge ist wichtig!)
	Mit einem Rechtsklick wird der zuletzt gesetzte Punkt wieder entfernt.
	Dieser Ausschnitt wird nun durch Drücken der Leertaste in ein Rechteck transfomiert. 
	Um den Ausschnitt direkt zu speichert, drücke Enter.  
	Um den Ausschnitt zu binarisieren, drücke Leertaste.
	Wenn das Ausschneiden wiederholt werden soll, drücke eine beliebige andere Taste.
	
	ACHTUNG!   Es müssen alle vier Ecken gesetzt werden, bevor abgebrochen werden kann!
	
	Nun wird der Ausschnitt ggf. noch binarisiert, um beim Drucken Toner zu sparen. 
	Der erste Schieberegler steht für die Fläche, über die adaptiv binarisiert wird.
	Der zweite verschiebt den kalkulierten Schwellwert nach oben.
	Mit dem dritten Regler kann die Stärke des anschließenden Weichzeichners eingestellt werden.
	
	Die voreingestellten Werte funktionieren in der Regel sehr gut, aber man kann eventuell 
	noch eine bessere Einstellung für jedes Bild finden. (Einfach ausprobieren!)
	
	Durch Drücken der Enter-Taste wird die Aufarbeitung bestätigt und das fertige Bild in der 
	"Ausgangsdatei" gespeichert.
	
	Jetzt kann ein neues Bild aufbearbeitet werden.
	"""
	Label(root, text=text, font = "Times 10", justify=LEFT, padx=10).grid(row=6, column=0,columnspan=2)

	
	root.mainloop()

	
	
	
	
	
	
	
